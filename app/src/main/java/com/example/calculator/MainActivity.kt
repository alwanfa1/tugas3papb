package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import java.util.*
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val textView : TextView = findViewById(R.id.textView)
        val button0 : Button= findViewById(R.id.button0)
        val button1 : Button= findViewById(R.id.button1)
        val button2 : Button= findViewById(R.id.button2)
        val button3 : Button= findViewById(R.id.button3)
        val button4 : Button= findViewById(R.id.button4)
        val button5 : Button= findViewById(R.id.button5)
        val button6 : Button= findViewById(R.id.button6)
        val button7 : Button= findViewById(R.id.button7)
        val button8 : Button= findViewById(R.id.button8)
        val button9 : Button= findViewById(R.id.button9)
        val buttonPlus : Button = findViewById(R.id.buttonPlus)
        val buttonMinus : Button = findViewById(R.id.buttonMinus)
        val buttonMultiplied : Button = findViewById(R.id.buttonMultiplied)
        val buttonEquals : Button = findViewById(R.id.buttonEquals)
        val buttonDiv:Button = findViewById(R.id.buttonDiv)
        val buttonClear : Button = findViewById(R.id.buttonC)
        val arrSym = arrayOf(":","*","+","-")
        val numbButtons = arrayOf(button0,button1,button2, button3, button4, button5, button6, button7, button8, button9, buttonMinus, buttonMultiplied,buttonPlus,buttonDiv)
        for (x in 0..13){

            numbButtons[x].setOnClickListener{
                if (textView.text.toString() == "0" &&!(numbButtons[x].text.toString() == "+" || numbButtons[x].text.toString() == "-" || numbButtons[x].text.toString() == ":" || numbButtons[x].text.toString() == ":")){
                    textView.text=x.toString()
                }else {
                    if(!(Arrays.stream(arrSym).anyMatch{t -> t == textView.text.toString()[textView.text.toString().length-1].toString()}&&Arrays.stream(arrSym).anyMatch{t -> t == numbButtons[x].text.toString()})){
                        textView.text = textView.text.toString().plus(numbButtons[x].text.toString())
                    }


                }
            }
        }
        buttonClear.setOnClickListener{
            textView.text=""
        }
        buttonEquals.setOnClickListener{
            val rhino:Context=Context.enter()
            var finalResult:String=""
            rhino.setOptimizationLevel(-1)
            try {
                val scriptable:Scriptable=rhino.initSafeStandardObjects();
                finalResult= rhino.evaluateString(scriptable,textView.text.toString(),"javascript",1,null).toString()
            }catch (e:Exception){
                finalResult="0"
            }
            textView.text=finalResult
        }

    }


}

